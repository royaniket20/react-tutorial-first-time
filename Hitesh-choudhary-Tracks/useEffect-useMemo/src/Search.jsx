import { memo } from "react";

//This component will get re rendred whenever count is getting Updated
function Search({onChangeCaller}) { //Passing Prop 
  console.log(`Search is being Rendered ---`);


  return (
    <>

<input type="text" className="w-80 h-10 mb-4 rounded-md border-2 font-mono text-2xl border-blue-950" onChange={onChangeCaller}></input>
    </>
  );
}

//If Not required It will Not re render - But for that we need to make sure onChangeFunction
//reference Do not get changed
export default memo(Search);
