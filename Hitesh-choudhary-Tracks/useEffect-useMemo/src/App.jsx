import { useState } from "react";
import { useMemo } from "react";
import { useCallback } from "react";
import Search from "./Search";
import { initialItems, nameArr } from "./initialItems";
import "./App.css";
//This component will get re rendred whenever count is getting Updated
function App() {
  console.log(`App is being Rendered ---`);
  // console.log(`Inital Items ------`);
  // console.log(initialItems);
  // console.log(`Names array-----`);
  // console.log(nameArr);
  const [names, setNames] = useState(nameArr);

  const [count, setCount] = useState(0);
  const [items, setItems] = useState(initialItems);
  //This can be a costly Operation --- When we try to iterate a very large array - we really can cahe this - it items array is Not get changed
  // const selectedItem = items.find((data) => data.isItemSelected && data.callDummy("nonCache"));
  //This will get called only when items changed
  //const selectedItemCachedWithCount  = useMemo(()=> items.find((data) => data.id === count),[items , count]); //This is an absolute Not an use case of useMemo - because by default count will increase - so Anyway you have to recompute
  const selectedItemCachedWithCount = useMemo(
    () => items.find((data) => data.id === count),
    [items]
  ); //This is Buggy Code - BVecause when Count change as its Not part of deps it will Not change
  const selectedItemCached = useMemo(
    () => items.find((data) => data.isItemSelected && data.callDummy("Cached")),
    [items]
  );
  function handleIncrementData() {
    //Changing the deps array to see if useMemo get called again
    if (count % 10 === 0) {
      // items.push({ id : 999,
      //   isItemSelected : false,
      //   callDummy : (data)=>{
      //     console.log(`Calling from : ${data}`);
      //     return true;
      //   },
      //   value :"Some Dummy Value"})
      console.log(`Updating Items to see if UeMemo get recalled `);
      let newItems = [...items]; // You shiud create new Array and set it so that React can detect the change and re render it
      setItems(newItems); //Now Ites is changed - Just adding value to Items is Not enough
    }

    setCount((count + 1) % items.length);
  }
  const getList = names.map((element,index) => {
    return <div className="text-blue-950 text-4xl m-2 font-mono bg-yellow-100 border-2 border-black" key={index}>{element}
    </div>;
  });

  // We can wrap it inside useCallback so that - when pass to Search Component 
  //It will Not create New Instance Until the Deps get changed 
 function doSomething(event){
       console.log(event.target.value);
       console.log(`To showcase that state refs also freeze in time === ${names[0]}`); //This will give bad behaviour when useCallback used 
        //Solution to the above problem is - put this as deps 
       if(event.target.value.length === 0 ){
        let newNames = [... nameArr];
        setNames(newNames);
       }else{
        let newNames = nameArr.filter(item=>item.startsWith(event.target.value));
        console.log(newNames);
        setNames(newNames);
      } 
  }
  //This will everytime create new Function defination - So we can say Search component will always get rendered 
  //As memo will always get diff function reference in search component 
  let doSomethingNormal = (event)=>doSomething(event);
  //Because we are using useCallback - Function defination along with all its used state variables 
  //Are freezed in Time - So in case you want to recreate Function for state change - add them in Deps 
  let doSomethingCached = useCallback((event)=>doSomething(event),[names]);

  return (
    <>
      <div className="bg-violet-500 flex w-100 h-full justify-center items-center flex-col">
        <div>
          <h1 className="font-bold text-3xl m-3">
            Demo of useCallback and useMemo
          </h1>
        </div>
        <div>
          <h1 className="font-bold text-3xl m-3">Count : {count} </h1>
          <h1 className="font-bold text-3xl m-3">
            Selected Item :{/*  {selectedItem.value} */}{" "}
            {selectedItemCachedWithCount.value}
          </h1>
          <h1 className="font-bold text-3xl m-3">
            Selected Cached Item : {selectedItemCached.value}
          </h1>
          <button
            className="bg-black p-3 text-white m-4 text-xl rounded-md"
            onClick={handleIncrementData}
          >
            Increment Count
          </button>
        
        </div>
        <div>
       {/*  Passing Function as Prop */}
      {/*    <Search onChangeCaller={doSomethingNormal}></Search>   */}
          <Search onChangeCaller={doSomethingCached}></Search>  
        </div>
        <div className="flex flex-wrap flex-row">
            {getList}
        </div>
      </div>
    </>
  );
}

export default App;
