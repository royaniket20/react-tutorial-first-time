import  Jabber   from "jabber";
const jabber = new Jabber();
const nameArr = [];
for (let index = 0; index < 100; index++) {
  const nameNoSalutation = jabber.createFullName(false);
  nameArr.push(nameNoSalutation);
}



const  initialItems = new Array(100).fill(0).map((_,index) =>{
  return {
    id : index,
    isItemSelected : index === 7,
    callDummy : (data)=>{
      console.log(`Calling from : ${data}`);
      return true;
    },
    value : 'Value_'+(_+index)+' '+nameArr[index% nameArr.length]
  }
});

export  {initialItems ,nameArr};