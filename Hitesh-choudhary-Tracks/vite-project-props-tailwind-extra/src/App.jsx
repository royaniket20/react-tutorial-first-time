import './App.css'
import Card from "./components/Card";

function App() {

 let myObj = {name : "aniket"}
 let myArr = [1,2,3,4,5]
  return (
    <>
      <div>
        {/*  className equivalent with class in html */}
        <h1 className='bg-green-500  p-4 rounded-xl'>Tailwind Test</h1>
      </div>
    {/*   How we are going to differe Content - Using props  */}
  {/*  <Card channle="Aniket" someObj = {myObj} myArr = {myArr}  /> */}
   <Card userName = "Aniket"/>
   <Card/>{/*  Forgot to send props - How to handle - you can use normal JS 
   But in React you can do better  - Set default value in props as method parameter   */}
    </>
  )
}

export default App
