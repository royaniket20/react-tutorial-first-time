import { useEffect, useState } from "react";
// import InputBox from './components/InputBox'
import { InputBox } from "./components";
import { useCurrencyConversionInfo } from "./hooks/useCurrencyInfo";
function App() {
  console.log(`App getting rendered -----------------------`);
  const [amount, setAmount] = useState(1);
  const [msg, setMsg] = useState("");
  const [isDisabled, setIsDisabled] = useState(false);
  const [currencyNames, setCurrencyNames] = useState({});
  const [from, setFrom] = useState("usd");
  const [to, setTo] = useState("inr");
  const currencyRateInfo = useCurrencyConversionInfo(
    from,
    setMsg,
    setIsDisabled,
    from,
    to
  ); //Whenever from  state get changed - It will trigger useEffect inside this Function
  const currencyOptions = Object.keys(currencyRateInfo);
  const [convertedAmount, setConvertedAmount] = useState(0);

  const swapCurrenciesToAndFrom = () => {
    console.log(`Swapping Function is Called `);
    setFrom(to);
    setTo(from);
    setConvertedAmount(0);
    setAmount(1);
  };
  const onConvertFunc = () => {
    console.log(`Called for Currency Conversion`);
    setConvertedAmount(amount * currencyRateInfo[to]);
  };

  useEffect(() => {
    console.log(`Called useEffect to Populate currenyFullNameCached`);
    fetch(
      `https://cdn.jsdelivr.net/npm/@fawazahmed0/currency-api@latest/v1/currencies.json`
    )
      .then((resp) => resp.json())
      .then((jsonData) => setCurrencyNames(jsonData));
  }, []);

  return (
    <>
      <div className="bg-gradient-to-br from-indigo-400 to-cyan-400  w-screen h-screen flex flex-col justify-center items-center">
        <div className="bg-gradient-to-t from-teal-200 to-teal-500 border-2 border-black text-5xl m-4 p-4 font-mono rounded-md">
          Currency Converter
        </div>
        <div className="shadow-[rgba(0,_0,_0,_0.8)_0px_30px_90px] border-2 border-white bg-gradient-to-b from-slate-500 to-slate-800 w-auto h-auto flex flex-col justify-center items-center p-8 rounded-md">
          <InputBox
            label="From"
            amount={amount}
            currencyOptions={currencyOptions}
            currencyOptionsFullNames={currencyNames}
            onCurrencyChange={(currency) => {
              console.log(`Selected Curreny to change From - ${currency}`);
              setFrom(currency);
            }}
            onAmountChange={(amount) => setAmount(amount)}
            selectdCurrency={from}
            cssClass="bg-gradient-to-r from-teal-400 via-green-200 to-blue-400"
          ></InputBox>
          <div className="relative inline-block text-lg group">
            <span className="relative z-10 block px-5 py-3 overflow-hidden font-medium leading-tight text-gray-800 transition-colors duration-300 ease-out border-2 border-gray-900 rounded-lg group-hover:text-white">
              <span className="absolute inset-0 w-full h-full px-5 py-3 rounded-lg bg-gray-50"></span>
              <span className="absolute left-0 w-48 h-48 -ml-2 transition-all duration-300 origin-top-right -rotate-90 -translate-x-full translate-y-12 bg-gray-900 group-hover:-rotate-180 ease"></span>
              <button className="relative" onClick={swapCurrenciesToAndFrom}>
                Switch
              </button>
            </span>
            <span
              className="absolute bottom-0 right-0 w-full h-12 -mb-1 -mr-1 transition-all duration-200 ease-linear bg-gray-900 rounded-lg group-hover:mb-0 group-hover:mr-0"
              data-rounded="rounded-lg"
            ></span>
          </div>
          <InputBox
            label="To"
            currencyOptions={currencyOptions}
            currencyOptionsFullNames={currencyNames}
            onCurrencyChange={(currency) => {
              console.log(`Selected Curreny to change To - ${currency}`);
              setTo(currency);
            }}
            selectdCurrency={to}
            amount={convertedAmount}
            amountDisabled={true}
            cssClass="bg-gradient-to-r from-purple-300 via-pink-300 to-red-300"
          ></InputBox>

          <div className="relative inline-block text-lg group">
            <span className="relative z-10 block px-5 py-3 overflow-hidden font-medium leading-tight text-gray-800 transition-colors duration-300 ease-out border-2 border-gray-900 rounded-lg group-hover:text-white">
              <span className="absolute inset-0 w-full h-full px-5 py-3 rounded-lg bg-gray-50"></span>
              <span className="absolute left-0 w-48 h-48 -ml-2 transition-all duration-300 origin-top-right -rotate-90 -translate-x-full translate-y-12 bg-gray-900 group-hover:-rotate-180 ease"></span>
              <button
                disabled={isDisabled}
                className="relative capitalize"
                onClick={onConvertFunc}
              >
                {msg}
              </button>
            </span>
            <span
              className="absolute bottom-0 right-0 w-full h-12 -mb-1 -mr-1 transition-all duration-200 ease-linear bg-gray-900 rounded-lg group-hover:mb-0 group-hover:mr-0"
              data-rounded="rounded-lg"
            ></span>
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
