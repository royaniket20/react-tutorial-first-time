/**
 * This is the Component which will have Dropdown and Inpout Field
 */

import { useId } from "react";

function InputBox({
  label,
  amount=0,
  onAmountChange,
  onCurrencyChange,
  currencyOptions = ["usd" , "inr"],
  selectdCurrency = "usd",
  amountDisabled = false,
  currencyOptionDisabled = false,
  cssClass,
  currencyOptionsFullNames 
}) {
  const amountInputId = useId ();
  //console.log(` Unique Id generated for accessibility - ${amountInputId}`);
  return (
    <>
      <div className="border-2 border-black w-8/10 h-auto p-1 m-3 bg-orange-100 flex rounded-md  text-2xl font-mono">
        <div className="flex flex-col">
          <label className="ml-1" htmlFor={amountInputId}>{label}</label>
          <input id={amountInputId} disabled={amountDisabled} className={`border-2 border-black rounded-md m-1 w-80 h-10 ${cssClass}`} type="number" value={amount} onChange={(e)=> onAmountChange && onAmountChange(Number(e.target.value))}></input>
        </div>
        <div className="flex flex-col">
          <label className="ml-1 mr-1">Currency Option</label>
          <select disabled={currencyOptionDisabled} className=" border-2 border-black rounded-md m-1 w-auto h-10" value={selectdCurrency} onChange={(e)=>onCurrencyChange && onCurrencyChange(e.target.value)}>
            {currencyOptions.map(( currency , index)=>{
              return <option key={index} value={currency}>{  currencyOptionsFullNames[currency] }</option>
            })}
               
          </select>
        </div>
      </div>
    </>
  );
}

export default InputBox;
