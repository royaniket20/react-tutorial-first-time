import { useEffect, useState } from "react";
//https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies.json
//https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/latest/currencies/usd.json
//Custom Hooks
function useCurrencyConversionInfo(currency , setMsg ,setIsDisabled , from , to , ) {
  console.log(`useCurrencyConversionInfo hook get called with curreny - ${currency}`);
  // usd , inr , eur etc ...
  const [data, setData] = useState({}); //Default empty Obj - Just to prevent Logic breaking
  //We want to make api call here - when This Hook get called
  useEffect(() => {
    //UI looking Bad 
    // setData({}); // Data will be Blank - So Until we get fresh Data - It will Not give wrong prewvious Data 
    setMsg("Loading Curreny Rates")
    setIsDisabled(true)
    fetch(
      `https://cdn.jsdelivr.net/npm/@fawazahmed0/currency-api@latest/v1/currencies/${currency}.json`
    )
      .then((resp) => resp.json())
      .then((jsonData) =>{  
        setData(jsonData[currency])
        setMsg(`Convert ${from} To ${to}`)
        setIsDisabled(false)
      });
    console.log(`Parsed Data for ${currency}  : ${data}`);

    
  }, [currency]); //This Hook will get called first time on Mounting and then when currency value change
   console.log(`useCurrencyConversionInfo Returning back with Data - ${data}`);
  return data; //Getting data back from the Method - Which will be populated by useEffect later time when fetch resolves 
}

export  {useCurrencyConversionInfo}; //Exporting the Whole Hook
