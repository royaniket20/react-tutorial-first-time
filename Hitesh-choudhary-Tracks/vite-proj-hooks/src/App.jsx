import { useState } from "react";
import "./App.css";

function App() {
  // let counter = 5;
  let [counter, setCounter] = useState(5);
  const addValue = () => {
    console.log(`Add value is called on ${counter}`);
    // counter++;
    //console.log(`Updated Value - ${counter}`);
    //useState(defaultValue) ==> Return an Array [one variable holding the current value , function  that contols the variable ]
    // setCounter(counter) //This will let react react on the state change

    //Here we can use previous state and callback - Now each of the call back will get executed in serial mode
    setCounter((prev) => {
      console.log(`Updating value of ${prev}`);
      return prev + 1;
    });
    setCounter((prev) => {
      console.log(`Updating value of ${prev}`);
      return prev + 1;
    });
    setCounter((prev) => {
      console.log(`Updating value of ${prev}`);
      return prev + 1;
    });
    setCounter((prev) => {
      console.log(`Updating value of ${prev}`);
      return prev + 1;
    });
  };
  const minusValue = () => {
    console.log(`Add value is called on ${counter}`);
    //counter--;
    console.log(`Updated Value - ${counter}`);
    //useState(defaultValue) ==> Return an Array [one variable holding the current value , function  that contols the variable ]
    // setCounter(counter) //This will let react react on the state change
    //Although you are using setCounter multiple time - But Fiber send it in batches
    //This Will create a single batch
    setCounter(counter - 1);
    setCounter(counter - 1);
    setCounter(counter - 1);
    setCounter(counter - 1);
  };
  return (
    <>
      <div>
        <h1>Counter Game</h1>
        <h2>Counter Value : {counter}</h2>
        <button onClick={addValue}>Add Value to {counter}</button>
        <br></br>
        <br></br>
        <button onClick={minusValue}>Minus Value to {counter}</button>
      </div>
    </>
  );
}

export default App;
