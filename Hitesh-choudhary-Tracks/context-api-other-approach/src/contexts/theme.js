import {createContext , useContext} from 'react'

const ThemeContext = createContext({
  themeMode : "light",
  darkTheme  : () =>{} ,
  lightTheme : ()=>{},
}); //We can add a Default Value , Function  also 

export default ThemeContext;

//Below Line is Equivaluen Of - This Commented section 
// const UserContextProvider = ({children}) =>{
//   const [user , setUser] = useState(null);
//   return (
//     /* value is an prop which can be used to pass whatever you need to pass */ 
//     <UserContext.Provider value={{user , setUser}}>
//       {children}
//     </UserContext.Provider>
//   )
// }
//export default UserContextProvider;

export const ThemeContextProvider = ThemeContext.Provider; //Value props will be given during usage 
//Here we create a Hook So that Just Like below we need not to Import useContext as well as 
//UserContext multiple time when Read need to be done 
//We can directly Import this useTheme and start using context  data or context provider data 

/* import  { useContext } from 'react'
import UserContext from '../context/UserContext';

function Outsider() {
  const someVal = useContext(UserContext); 
 ...
} */
export const useTheme = ()=>{
  return  useContext(ThemeContext);
}