/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {},
  },
  darkMode : "class", // So that we can add "dark" , "light" class at html  tag and then using them switch theme 
  plugins: [],
}

