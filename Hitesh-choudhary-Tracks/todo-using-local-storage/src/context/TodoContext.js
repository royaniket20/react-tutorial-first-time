import { createContext  , useContext} from "react";

//Context Creation 

//Having some default values also It shoud have Functions also for Resuse in project
//Method functionality I will use whever needed  - if a parent component define it - Child will have easily access 
export const TodoContext = createContext({
todos : [
{
 id : 1 , 
 todo : "Some Msg 1" ,
 completed : false 
}] , 
addTodo : (todo)=> { console.log(`To be Implemented by other - ${todo}`);},
updateTodo : (id , todo)=> {console.log(`To be Implemented by other - ${todo} , ${id}`);},
deleteTodo : (id)=> {console.log(`To be Implemented by other - ${id}`);},
toggleComplete : (id) => {console.log(`To be Implemented by other - ${id}`);}
});

//Creating of Provider 
export const TodoContextProvider = TodoContext.Provider;

//Create Context Use Wrapper 
export const useTodoContext = ()=>{
  return useContext(TodoContext);
}