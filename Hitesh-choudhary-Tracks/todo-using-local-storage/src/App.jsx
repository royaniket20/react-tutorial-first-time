import { useState , useEffect } from 'react'
 import { TodoContextProvider } from "./context"; //Index.js is used - so No need to add file name 
 import { TodoForm, TodoItem } from "./components";
import './App.css'

function App() {
  const [todos, setTodos] = useState([])
 
  //Loading inital Todos from Local Storage

  //Using Multiple UseEffect  - One for Loading Data from Local staorage 
  useEffect (()=> {
   const existingTodos = JSON.parse(localStorage.getItem("todos"));
   if(existingTodos && existingTodos.length) //To Know if its Json array Or Not 
   {
    setTodos(existingTodos);
   }
   console.log(" This use effect called To Load inital Data  ");
  } , [])

  //One we will Update LocalDataStorage with recent data whenever Todos array will get updated by some operation 

  useEffect (()=> {
    localStorage.setItem("todos" , JSON.stringify(todos))
    console.log(" This use effect called as todos array modified ");
   } , [todos])

   //pverride default impls in App jsx 


  const addTodo = (todo)=> {
    setTodos((exisiting)=> {
      //aasumition is this todo Obj do not have Id field  - else first destructure and then add Id to overwrite 
      let obj = {
        id : Date.now() , ... todo
      }
      exisiting.push(obj);
      console.log(`Add Todo Data - ${JSON.stringify(exisiting)}`);
     return [...exisiting ]; //This is required as if We do not send new Array - Re render will not happen
    })
  }

  const updateTodo = (id , todo)=> {
    setTodos((exisiting)=> {
     const newArr = exisiting.map((eachItem) => eachItem.id === id ? todo : eachItem )
     console.log(`Update  Todo Data for Id ${id} - ${JSON.stringify(newArr)}`);
     return newArr;

    })
  }
  const deleteTodo = (id)=> {
    setTodos((exisiting)=> {
      const newArr =exisiting.filter((eachItem) => eachItem.id !== id )
      console.log(`Delete   Todo Data for Id ${id} - ${JSON.stringify(newArr)}`);
      return newArr;

     })
  }
  const toggleComplete = (id) => {
    setTodos((exisiting)=> {
      const newArr =exisiting.map((eachItem) => eachItem.id === id ? {...eachItem , completed : !eachItem.completed} : eachItem )
      console.log(`Toggle  Todo Data for Id ${id} - ${JSON.stringify(newArr)}`);
      return newArr;
    })
  }
  return (
    // Here we are wrpping whole app using Context Provider 
    <TodoContextProvider value={{todos, addTodo , deleteTodo , toggleComplete ,updateTodo}}>
   <div className="bg-[#172842] min-h-screen py-8">
                <div className="w-full max-w-2xl mx-auto shadow-md rounded-lg px-4 py-3 text-white">
                    <h1 className="text-2xl font-bold text-center mb-8 mt-2">Manage Your Todos</h1>
                    <div className="mb-4">
                      {/* There is no prop drilling as we are using context wrapper  */}
                       <TodoForm></TodoForm>
                    </div>
                    <div className="flex flex-wrap gap-y-3">
                        {/*Loop and Add TodoItem here */}
                        {todos.map((todo)=>(
                          <div key={todo.id} className='w-full'>
                               <TodoItem todoItem = {todo} />
                          </div>
                        ))}
                    </div>
                </div>
            </div>
    </TodoContextProvider>
  )
}

export default App
