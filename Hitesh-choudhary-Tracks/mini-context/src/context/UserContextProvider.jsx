import React, { useState } from "react";
import UserContext from "./UserContext.js";
//hERE CHILDREN  means whatever this tag is wrappuing - refer to app.jsx 
const UserContextProvider = ({children}) =>{
  const [user , setUser] = useState({name : "Provider overridden Name"});
  const dummyData = "I am updated Dummy Data";
  return (
    /* value is an prop which can be used to pass whatever you need to pass  - This will override default value of Context */ 
    <UserContext.Provider value={{dummyData, user , setUser}}>
      {children}
    </UserContext.Provider>
  )

}

export default UserContextProvider;

