import React, { useContext, useEffect } from 'react'
import UserContext from '../context/UserContext';

function Profile() {
  const {user} = useContext(UserContext); 
   useEffect(()=>{
    console.log(`User Value is Changed - ${JSON.stringify(user)}`);
   },[user]);

  if(!user || !user.username){
    return (
      <h1>Please Login</h1>
    )
  }else {
    return (
      <div>
        <h1>Login Credentials</h1>
        <h3>{
        JSON.stringify(user)
        
        }</h3>
      </div>
    )
  }
 
}

export default Profile