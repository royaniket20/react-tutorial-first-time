import React, { useContext, useState } from 'react'
import UserContext from '../context/UserContext'

function Login() {
  const [username , setUsername] = useState("")
  const [password , setPassword] = useState("")
  //nOW HERE YOU HAVE ALL THE THINGS ACCESSIABLE WHICH YOU HAVE SET INSIDE PROVIDER 
  const {dummyData,setUser ,user} = useContext(UserContext); 
  function  handleLogin(event){
    event.preventDefault();
    console.log(`Login Button is Clicked `);
    let json = {username , password};
    console.log(`Sending Data via Context api - ${JSON.stringify(json)}`);
    setUser(json)
  }

  return (
    <div>
    <h2>Login - To Write Data via Context APi DummyData Updated- {dummyData} || User Value = {JSON.stringify(user) }</h2>
       <input type='text'  value = {username} placeholder='username' onChange={(event)=> setUsername(event.target.value)}/>
       <input type='text'  value={password} placeholder='password'   onChange={(event)=> setPassword(event.target.value)}/>
       <button onClick={handleLogin}>Login</button>
    </div>
  )
}

export default Login