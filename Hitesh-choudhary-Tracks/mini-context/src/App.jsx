import { useState } from 'react'
import './App.css'

import UserContextProvider from './context/UserContextProvider'
import Login from './Login/Login'
import Profile from './Profile/Profile'
import Outsider from './Outsider/Outsider'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
    <UserContextProvider>
     {/*  Whatever componet we will put here will have access to the UserContext Context  */}
      <div>
         <h1>Reach with Context Api - State Management</h1>
         <Login/>
         <Profile/>
      </div>
      </UserContextProvider>
      {/* I am someone Outside provider - So I will have access to Default value of the context  */}
      <Outsider></Outsider>
    </>
  )
}

export default App
