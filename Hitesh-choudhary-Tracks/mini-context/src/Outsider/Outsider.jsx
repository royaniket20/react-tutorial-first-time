import  { useContext } from 'react'
import UserContext from '../context/UserContext';

function Outsider() {
  const someVal = useContext(UserContext); 
    return (
      <h1>UserContext Default Vaue - Outside Provider - {someVal.dummyData} | User Value ?? {someVal.user? someVal.user : "Not Available" }</h1>
    )
 
}

export default Outsider