const mainContainer = document.getElementById("root");

//how react give the element out of jsx function

//React eventually send a Object represent an Object Json 
const reactElement =  {
  type : 'a',
  props : {
    href : 'https://www.google.com',
    targer : '_blank'
  },
  children : 'Click Me to Visit'
}

function customRender(reactElement,mainContainer){
  let docElement = document.createElement(reactElement.type);
  docElement.innerHTML = reactElement.children;
  for (const key in reactElement.props) {

    console.log(`${key}: ${reactElement.props[key]}`);
    docElement.setAttribute(key,reactElement.props[key]);
    mainContainer.appendChild(docElement);
}
 
}
//Now react have an Method that will render the Object that jsx method 
//to the Html

customRender(reactElement,mainContainer)