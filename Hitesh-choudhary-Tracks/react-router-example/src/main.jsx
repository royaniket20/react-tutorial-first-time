import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import { BrowserRouter ,Route  ,RouterProvider, createBrowserRouter  , createRoutesFromElements} from "react-router-dom";
import Layout from "./Layout.jsx";
import Home from "./components/Home/Home.jsx";
import About from "./components/About/About.jsx";
import Contact from "./components/Contact/Contact.jsx";
import User from "./components/User/User.jsx";

//This is One way of creating React Router Dom
const routerWithJson = createBrowserRouter([
  {
    path: "/",
    element: <Layout></Layout>,
    children: [
      {
        path: "",
        element: <Home></Home>,
      },
      {
        path: "about",
        element: <About></About>,
      },
      {
        path: "contact",
        element: <Contact></Contact>,
      },
      {
        path: "user/:userId",
        element: <User></User>,
      },
    ],
  },
]);
//Another way of creating React Router 
const routerWithTag = createBrowserRouter(
     createRoutesFromElements(
      <Route  path='/' element={<Layout/>}>
        <Route  path='' element={<Home/>}/>
        <Route  path='about' element={<About/>}/>
        <Route  path='contact' element={<Contact/>}/>
       {/*  Inside this component We can use userId */}
        <Route  path='user/:userId' element={<User/>}/>
      </Route >

     )
  );
ReactDOM.createRoot(document.getElementById("root")).render(
//   <React.StrictMode>
//   {/*   You can Use Router in App  Component alos  */}
// {/*   <BrowserRouter>
//     <App />
//   </BrowserRouter> */}
//     {/*   We can use React Router Here directly  */}
//    {/*  <RouterProvider router={routerWithJson}></RouterProvider> */}
// {/*    When you are using loader enabled Router in app and returning RouterProvider */}
// <App />
//   </React.StrictMode>
<App />
);
