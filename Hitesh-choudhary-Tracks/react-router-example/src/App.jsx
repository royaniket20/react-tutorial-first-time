import "./App.css";
import Layout from "./Layout";
import About from "./components/About/About";
import Contact from "./components/Contact/Contact";
import Home from "./components/Home/Home";
import { Route, Routes } from "react-router-dom";
import User from "./components/User/User";
import Github from "./components/Github/Github";
import FastGithub, { githubInfoLoader } from "./components/Github/Fast/FastGithub";
import SlowGithub from "./components/Github/Slow/SlowGithub";
import {
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider
} from "react-router-dom";

const RouteJsx = (

  <Route path="/" element={<Layout />}>
    <Route path="" element={<Home />} />
    <Route path="home" element={<Home />} />
    <Route path="about" element={<About />} />
    <Route path="contact" element={<Contact />} />
    <Route path="user/:userId" element={<User />} />
    <Route path="user/" element={<User />} />
    <Route path="github"  loader={githubInfoLoader}  element={<Github />}>
      <Route path="" element={<FastGithub />} />
      <Route path="fast" element={<FastGithub />} />
      <Route path="slow" element={<SlowGithub />} />
    </Route>
  </Route>
);
const routes = createRoutesFromElements(RouteJsx);

const router = createBrowserRouter(routes);
function App() {
  /**
   * I don't think this is possible. According to docs you must use RouterProvider in order to enable rest of the apis
   */
    {/*  loader is not possible with this way of Routing  */}
/*   return (
    <>
    <Routes>
        <Route path="/" element={<Layout />}>
          <Route path="" element={<Home />} />
          <Route path="home" element={<Home />} />
          <Route path="about" element={<About />} />
          <Route path="contact" element={<Contact />} />
          <Route path="user/:userId" element={<User />} />
          <Route path="user/" element={<User />} />
          <Route path="github"  loader={githubInfoLoader}  element={<Github />}>
            <Route path="" element={<FastGithub />} />
            <Route path="fast" element={<FastGithub />} />
            <Route path="slow" element={<SlowGithub />} />
          </Route>
        </Route>
      </Routes> 
    </>
  ); */
 /*  Correct way to Sending loader enabled routes  */
 return <RouterProvider router={router} />;
}



export default App;
