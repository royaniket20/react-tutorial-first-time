import React, { useState ,useEffect } from 'react'
import { useParams, useLocation,Routes,Route,Outlet ,useLoaderData } from "react-router-dom";
import Gitfooter from './Footer/Gitfooter';
import Githeader from './Header/Githeader';


function Github() {
  const [gitData , setGitData] = useState([]);
  const data = useLoaderData(); // Data will get loaded even before UseEffect come into action 
  useEffect(() => {
    setGitData(data);
  }, [data])
  
  return (
   
    <div className='bg-orange-200 basis-11/12 justify-start items-center flex flex-col'>
      
    <h1 className='font-extrabold text-3xl'>I AM GITHUB</h1>
    <div className='bg-black w-full h-full flex flex-col justify-between'>
    <Githeader></Githeader>
    <Outlet context={[gitData , setGitData]}></Outlet>
    <Gitfooter></Gitfooter>
    </div>
    

  </div>
  )
}

export default Github