import React, { useState ,useRef, useEffect  } from 'react'
import { NavLink } from 'react-router-dom'

function Githeader() {
  return (
    <>
    <div className='bg-violet-300 row-span-1 flex flex-col justify-center items-center w-full'>
    <h1 className='font-extrabold text-3xl'>I AM Git HEADER</h1>
    <div className='bg-black h-full w-full flex justify-evenly'>
      <NavLink to={"fast"} className={({isActive,isPending,isTransitioning})=>{
       // console.log(`Value is isActive Home is - ${JSON.stringify(isActive)}`);
     
        return `${isActive ? "text-emerald-200" : "text-white"} text-xl font-extrabold`
      }}>FAST LOADING</NavLink>
       <NavLink to={"slow"} className={({isActive,isPending,isTransitioning})=>{
      //  console.log(`Value is isActive About US is - ${JSON.stringify(isActive)}`);
      return `${isActive ? "text-emerald-200" : "text-white"} text-xl font-extrabold`
      }}>SLOW LOADING</NavLink>
      </div>
    </div>
    </>
  )
}

export default Githeader 