import React from 'react'
import { useOutletContext } from 'react-router-dom'
function FastGithub() {
  //To get data from Loader
  const [gitData , setGitData] = useOutletContext();
  return (

    <div className='bg-orange-800 justify-space items-center flex flex-col h-full'>
      
    <h1 className='font-extrabold text-3xl bg-white w-full text-center'>I AM FAST GITHUB</h1>
    <div className='bg-yellow-300 h-full w-full flex justify-evenly items-center'>
     <div className='text-center m-4 bg-gray-600 text-white p-4 text-3xl flex flex-row justify-evenly w-full flex-wrap'>
{/*      {gitData.map((data , index) => (
         console.log(index , data.avatar_url)
        ))} */}
     {gitData.map((data , index) => (
          <img className='mt-3 rounded-2xl' key={index} src={data.avatar_url} alt="Git picture" width={300}/>
        ))}

   {/*  <img className='mt-3 rounded-2xl' src={gitData.avatar_url} alt="Git picture" width={300} />
    <img className='mt-3 rounded-2xl' src={gitData.avatar_url} alt="Git picture" width={300} />
    <img className='mt-3 rounded-2xl' src={gitData.avatar_url} alt="Git picture" width={300} />
    <img className='mt-3 rounded-2xl' src={gitData.avatar_url} alt="Git picture" width={300} />
    <img className='mt-3 rounded-2xl' src={gitData.avatar_url} alt="Git picture" width={300} />
    <img className='mt-3 rounded-2xl' src={gitData.avatar_url} alt="Git picture" width={300} />
    <img className='mt-3 rounded-.avatar_url} alt="Git picture" width={300} /> */}
    </div>
     </div>
  </div>
  )
}

export default FastGithub

export const githubInfoLoader = async () => {
 console.log(`Calling the Image Loader ------ for loading heavy images @ --`+new Date());
  const urls = [
    "https://images.unsplash.com/photo-1504814532849-cff240bbc503?q=80&w=2500&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
    "https://images.unsplash.com/photo-1541623089466-8e777dd05d70?q=80&w=2670&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
    "https://images.unsplash.com/photo-1541169477997-6ed649539ec4?q=80&w=2670&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
    "https://images.unsplash.com/photo-1493246507139-91e8fad9978e?q=80&w=2670&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
    "https://images.unsplash.com/photo-1504814532849-cff240bbc503?q=80&w=2500&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
    "https://images.unsplash.com/photo-1504814532849-cff240bbc503?q=80&w=2500&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
    "https://images.unsplash.com/photo-1511300276866-a284652b55c3?q=80&w=2670&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
    "https://images.unsplash.com/photo-1498534405718-957b7b84b009?q=80&w=2670&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
    "https://images.unsplash.com/photo-1504814532849-cff240bbc503?q=80&w=2500&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
    "https://images.unsplash.com/photo-1507936580189-3816b4abf640?q=80&w=2670&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
  ]
  let responses = [];
 let  i = 0;
  for (const element of urls) {
   let url =  await fetchImage(element);
   let urlKey =`avatar_url_${i++}`; 
  //  let resp = {[urlKey] : url};
  let resp = {avatar_url : url }
   responses.push(resp);
  }
//console.log(responses);
  return responses;
}

async function fetchImage(givenUri) {
  const myHeaders = new Headers();
  myHeaders.append("pragma", "no-cache");
  myHeaders.append("cache-control", "no-cache");

  const myInit = {
    method: "GET",
    headers: myHeaders,
  };
  const response = await fetch(givenUri,myInit)
  const blobData = await  response.blob()
  //console.log(`Fast Component Blob Data `);
  //console.log(blobData);
  const url = URL.createObjectURL( blobData );
  //console.log(`Fast Component Blob Url  `);
 // console.log(url);
  return url;
}