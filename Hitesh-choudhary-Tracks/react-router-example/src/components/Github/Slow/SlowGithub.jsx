import React, { useEffect, useState } from "react";
import { useParams, useLocation } from "react-router-dom";
import { githubInfoLoader } from "../Fast/FastGithub";

function SlowGithub() {
  const [dataSet, setDataSet] = useState([]);
  useEffect(() => {
    githubInfoLoader().then(resp=>{
    // console.log(resp);
      setDataSet(resp);
    })
  }, []);
  return (
    <div className="bg-orange-800 justify-space items-center flex flex-col h-full">
      <h1 className="font-extrabold text-3xl bg-white w-full text-center">
        I AM SLOW GITHUB
      </h1>
      <div className='bg-yellow-300 h-full w-full flex justify-evenly items-center'>
     <div className='text-center m-4 bg-gray-600 text-white p-4 text-3xl flex flex-row justify-evenly w-full flex-wrap'>
{/*      {dataSet.map((data , index) => (
          console.log(index , data.avatar_url)
        ))} */}
     {dataSet.map((data , index) => (
          <img className='mt-3 rounded-2xl' key={index} src={data.avatar_url} alt="Git picture" width={300}/>
        ))}
    </div>
     </div>
    </div>
  );
}

export default SlowGithub;
