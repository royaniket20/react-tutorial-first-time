import React from 'react'

function Gitfooter() {
  return (
    <>
    <div className='bg-stone-700 text-white row-span-1 flex justify-center items-center w-full'>
       
       <h1 className='font-extrabold text-3xl'>I AM Git FOOTER</h1>

    </div>
    </>
  )
}

export default Gitfooter