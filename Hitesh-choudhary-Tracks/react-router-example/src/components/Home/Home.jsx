import React from 'react'

function Home() {
  return (
    <div className='bg-amber-200 basis-11/12 justify-center items-center flex'>
      
      <h1 className='font-extrabold text-3xl'>I AM HOME SWEET HOME</h1>

    </div>
  )
}

export default Home