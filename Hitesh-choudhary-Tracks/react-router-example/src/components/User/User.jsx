import React from 'react'
import { useParams, useLocation,useOutletContext} from "react-router-dom";

function User() {
  const [name , setName] = useOutletContext()
  const {userId} = useParams();
  const location = useLocation();
  const { state } = location;

  return (
   
    <div className='bg-zinc-200 basis-11/12 justify-center items-center flex flex-col'>
      
    <h1 className='font-extrabold text-3xl'>USER VIA PARAM  =  {userId? userId : "Empty Param" } || VIA STATE - {state ? state : "Empty State"}</h1>
    <h1 className='font-extrabold text-3xl'>useOutletContext Updating Data  =  {name? name : "Empty" } </h1>

  </div>
  )
}

export default User