import React, { useState    } from 'react'
import { NavLink } from 'react-router-dom'

function Header({nameSetter}) {
 
  const [userName , setUserName] = useState("");
   function updateName(data){
    console.log(data.target.value);
    setUserName(data.target.value);
    nameSetter(data.target.value); //This will set the Name in callback 
   }

  return (
    <>
    <div className='bg-violet-300 row-span-1 flex flex-col justify-center items-center'>
    <h1 className='font-extrabold text-3xl'>I AM COMMON HEADER</h1>
    <div className='bg-black h-full w-full flex justify-evenly'>
      <NavLink to={""} className={({isActive,isPending,isTransitioning})=>{
       // console.log(`Value is isActive Home is - ${JSON.stringify(isActive)}`);
     
        return `${isActive ? "text-emerald-200" : "text-white"} text-xl font-extrabold`
      }}>Home</NavLink>
       <NavLink to={"/about"} className={({isActive,isPending,isTransitioning})=>{
      //  console.log(`Value is isActive About US is - ${JSON.stringify(isActive)}`);
      return `${isActive ? "text-emerald-200" : "text-white"} text-xl font-extrabold`
      }}>About Us</NavLink>
       <NavLink to={"/contact"} className={({isActive,isPending,isTransitioning})=>{
       // console.log(`Value is isActive Contact Us is - ${JSON.stringify(isActive)}`);
       return `${isActive ? "text-emerald-200" : "text-white"} text-xl font-extrabold`
      }}>Contact Us</NavLink>
       <NavLink to={"/github"} className={({isActive,isPending,isTransitioning})=>{
       // console.log(`Value is isActive Contact Us is - ${JSON.stringify(isActive)}`);
       return `${isActive ? "text-emerald-200" : "text-white"} text-xl font-extrabold`
      }}>GitHub</NavLink>
      <div className='flex justify-center items-center'>
        <input type='text' className='m-1' value={userName} onChange={(data)=> updateName(data)}></input>
      {/* WE  CAN USE STATE TO PASS  PROS to Calling Component */}
       <NavLink   to={`/user/${userName}`} state={userName}  className={({isActive,isPending,isTransitioning})=>{
       // console.log(`Value is isActive User is - ${JSON.stringify(isActive)}`);
        return `${isActive ? "text-emerald-200" : "text-white"} text-xl font-extrabold`
      }}>User</NavLink>
       </div>
    </div>
    </div>
    </>
  )
}

export default Header 