import React, { useState } from 'react'
import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'
import { Outlet } from "react-router-dom";
import Sidebar from './components/Sidebar/Sidebar';
function Layout() {
  const [name , setName] = useState("")
  function nameSetter(myname) {
    console.log(`Given Name - ${myname}`);
    setName(myname)
  }
  return (
    <>
    <div className='bg-blue-300 grid grid-flow-row grid-rows-12 h-screen'>   
    <Header nameSetter={nameSetter}></Header>
    <div className='row-span-10 flex'>
    <Sidebar></Sidebar>
     {/* Dynamically Pass Items Here  */}
     <Outlet context={[name , setName]} />
    </div>
    <Footer></Footer>
    </div>
    </>
  )
}

export default Layout