//This is the store file 

//Stoire need to be aware of all the reducers - So that they only can modify state 
//Others are not allowed to modify the state 
import { configureStore } from "@reduxjs/toolkit";
import todoReducer from "../features/todo/todoSlice";
export const store = configureStore({
  reducer : todoReducer, //You can Give multiple Reducer List 
}); //Blank Store created 

