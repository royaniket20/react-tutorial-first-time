
import './App.css'
import AddTodo from './components/AddTodo'
import Todos from './components/Todos'

import { Provider } from "react-redux";
import { store } from "./app/store";
function App() {


  return (
    <>
    <Provider store={store}>
   <div className="bg-gradient-to-r from-teal-400 to-yellow-200 w-screen h-screen">
     <h1 className='text-4xl pt-4'>Magic Of  Redux Toolkit</h1>
      <AddTodo></AddTodo>
      <Todos></Todos>
      </div>
      </Provider>
    </>
  )
}

export default App
