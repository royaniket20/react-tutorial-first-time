//This are Reducers or Slice in Toolkit
//nanoid is just a util for Unique Id generation
import { createSlice, nanoid } from "@reduxjs/toolkit";
//Its just an Object
const initialState = {
  todos: [
  ],
}; //Init state of the Store
//Slices will have Names - Will be useful for debugging
//YOU SHOUD NOT EXPORT LIKE THIS 

export const todoSlice = createSlice({
  name: "todo",
  initialState: initialState,
  reducers : {
    addTodo : (state , action)=> {
      //action.payload = one Object 
      //state = Refer to initialState Object structure 
     const todo =  {
      id: nanoid(),
      text: action.payload,
    }
    state.todos.push(todo); // Udating Sotre
    } ,
    removeTodo : (state , action) => {
      const todoId = action.payload;
      state.todos = state.todos.filter((item)=> item.id !== todoId); //Overwrittning Sotre 
    } , 
  }
});

//So that we can use the reducers  easily -  createSlice provide action Prop which Hold all the reducer functions ref 
export const {addTodo ,removeTodo} = todoSlice.actions; //Remember Sysntax 
export default todoSlice.reducer; // This can be Now accessed using import TodoReducer from "../features/todo/todoSlice";