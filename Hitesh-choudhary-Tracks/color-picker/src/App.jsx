import { useState } from "react";

function App() {
  const [color, setColor] = useState("olive");

  return (
    <>
      <div
        className="flex w-screen h-screen  justify-center items-center"
        style={{ backgroundColor: color }}
      >
        <div className="border-2 border-indigo-600 flex rounded-lg h-auto   w-auto text-yellow-100 text-sm bg-white p-2 mb-3 ml-3 mr-3 flex-wrap justify-evenly">
          <button
            onClick={() => setColor("red")}
            style={{ backgroundColor: "red" }}
            className="w-20 h-8  rounded-lg m-1"
          >
            Red
          </button>
          <button
            onClick={() => setColor("magenta")}
            style={{ backgroundColor: "magenta" }}
            className=" w-20 h-8  rounded-lg m-1"
          >
            Magenta
          </button>
          <button
            onClick={() => setColor("green")}
            style={{ backgroundColor: "green" }}
            className=" w-20 h-8  rounded-lg m-1"
          >
            Green
          </button>
          <button
            onClick={() => setColor("olive")}
            style={{ backgroundColor: "olive" }}
            className=" w-20 h-8  rounded-lg m-1"
          >
            Olive
          </button>
          <button
            onClick={() => setColor("violet")}
            style={{ backgroundColor: "violet" }}
            className=" w-20 h-8  rounded-lg m-1"
          >
            Violet
          </button>
          <button
            onClick={() => setColor("brown")}
            style={{ backgroundColor: "brown" }}
            className=" w-20 h-8  rounded-lg m-1"
          >
            Brown
          </button>
          <button
            onClick={() => setColor("blue")}
            style={{ backgroundColor: "blue" }}
            className=" w-20 h-8  rounded-lg m-1"
          >
            Blue
          </button>
          <button
            onClick={() => setColor("LightGreen")}
            style={{ backgroundColor: "LightGreen" }}
            className=" w-20 h-8  rounded-lg m-1"
          >
            Light Green
          </button>
          <button
            onClick={() => setColor("Gold")}
            style={{ backgroundColor: "Gold" }}
            className=" w-20 h-8  rounded-lg m-1"
          >
            Gold
          </button>
        </div>
      </div>
    </>
  );
}

export default App;
