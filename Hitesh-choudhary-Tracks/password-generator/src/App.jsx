import { useState, useEffect, useRef } from "react";
function App() {
  const [length, setLength] = useState(8);
  const [isNumberAllowed, setIsNumberAllowed] = useState(false);
  const [isTextAllowed, setIsTextAllowed] = useState(true);
  const [defaultPass, setDefaultPass] = useState(""); //We need to call password generator
  const passwordRef = useRef(null); //You can use it for Dom manipulation stuff
  /**
   * Password generator Function
   */
  const passwordGenerator = () => {
    console.log(`passowrd getting generated`);
    let pass = "";
    let str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    if (isNumberAllowed) {
      str += "1234567890";
    }
    if (isTextAllowed) {
      str += "!@#$%^&*";
    }

    for (let index = 0; index < length; index++) {
      let char = Math.floor(Math.random() * str.length + 1);
      pass += str.charAt(char);
    }
    console.log(`Generated password - ${pass}`);
    setDefaultPass(pass); //Setting the Default password - Which will change defaultPass everywhere
  };

  //This Hook ensure that whenever the deps get changed - Call the passwordGenerator again
  //This also get called when component first time get rendered 
  useEffect(() => {
    console.log(`UseEffect is calling passwordGenerator`);
    passwordGenerator(); //Non memorized Function
  }, [length, isNumberAllowed, isTextAllowed]);

  return (
    <>
      <div className=" flex justify-center items-center w-screen h-screen  bg-violet-300 ">
        <div className="flex flex-col justify-start items-center  transition ease-in-out delay-150 h-auto pb-1 ml-2 mr-2 w-full sm:w-1/2 md:w-1/2 2xl:w-1/2 xl:w-1/2 lg:w-1/2 md:w-1/2 bg-green-300 rounded-xl border-solid border border-red-900 hover:shadow-2xl hover:ml-3 hover:mr-3 hover:shadow-blue-950 hover:-translate-y-1 hover:scale-105">
          <div className="h-auto w-auto font-mono underline decoration-1 text-emerald-700 bg-yellow-200 rounded-md pt-1 pb-1 pl-2 pr-2 mt-1 mr-1 ml-1">
            <h1 className="text-center text-xl w-full">Password Generator</h1>
          </div>
          <div className="flex h-auto w-auto font-mono text-sm text-emerald-700 bg-sky-200 rounded-md pt-1 pb-1 pl-2 pr-2 mt-1 mr-1 ml-1">
            <input
              className=" bg-white rounded-md  w-full"
              type="text"
              readOnly
              value={defaultPass}
              ref={passwordRef}
            ></input>
            <button
              className="ml-1 pl-1 pr-1 text-sm  text-white  rounded-full bg-purple-600 active:bg-violet-700"
              onClick={() => {
                console.log(`Copy password to Clip Board - ${defaultPass}`);
                passwordRef.current?.select();
               // passwordRef.current?.setSelectionRange(3, 12);

                window.navigator.clipboard.writeText(defaultPass);
              }}
            >
              Copy
            </button>
          </div>
          <div className=" flex flex-col justify-center items-center h-auto w-auto font-mono text-sm text-emerald-700 bg-sky-200 rounded-md pt-1 pb-1 pl-2 pr-2 mt-1 mr-1 ml-1">
            <div className="flex justify-center w-full ">
              <input
                className="mr-2 w-full"
                type="range"
                min={8}
                max={32}
                value={length}
                onChange={(event) => {
                  setLength(event.target.value);
                }}
              ></input>
              <label>Length({length})</label>
            </div>
            <div className="flex flex-wrap justify-around w-full">
              <label>Numbers [{isNumberAllowed + ""}]</label>
              <input
                type="checkbox"
                checked={isNumberAllowed}
                onChange={() => setIsNumberAllowed(!isNumberAllowed)}
              ></input>
              <label>Special Chars [{isTextAllowed + ""}]</label>
              <input
                type="checkbox"
                checked={isTextAllowed}
                //yOU CAN  use this way
                // onChange={() => setIsTextAllowed(!isTextAllowed)}
                //Or you have access to previous state also which is same
                onChange={() =>
                  setIsTextAllowed((prevIsTextAllowed) => !prevIsTextAllowed)
                }
              ></input>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
