import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root")).render(
  /**
   * As part of React Strict Mode, certain lifecycle functions will be ran twice,
   * such as functions passed to useState, useMemo, or useReducer, or the whole body
   *  of a functional component, which might include your useEffect hook.
   */
  /*  <React.StrictMode> */
  <App />
  /*   </React.StrictMode>, */
);
