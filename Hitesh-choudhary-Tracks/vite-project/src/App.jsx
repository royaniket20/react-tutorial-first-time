import Testing from "./testing.jsx";

function App() {
  
  const name = 'Aniket Roy'
  // <></> this is needed when you want to return a component with multiple Tags 
  //You are allowed to return only one element 
 return( 
 //This is called fragment
 <>
  <Testing/>
   <h1>Hello My Mentor : Hitesh | Student : {name}</h1>
  </>

  );

}

export default App;
