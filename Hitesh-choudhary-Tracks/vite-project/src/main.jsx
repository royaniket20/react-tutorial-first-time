import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";

function MyApp() {
  return (
    <div>
      <h1>My Custom App !!</h1>
    </div>
  );
}

let  MyApp2 = (
    <div>
      <h1>My Custom App 2222 !!</h1>
    </div>
  )

//This will not work as we do not have our renderedre here - check in Hello world Project 
//ow we rendered mimic React Render function 
const ReactElement = {
  type: "a",
  props: {
    href: "https://www.google.com",
    targer: "_blank",
  },
  children: "Click Me to Visit",
};

/**
 * Key Differences:
Type: The first is a JSX element, while the second is a function (component).
Rendering: The first can be directly included in JSX using {}, while the second must be called as a component using <xxx/>.
Reusability: The functional component can accept props and be reused with different values, whereas the JSX element cannot.
In summary, use the first method for static content and the second method when you need a reusable component.
 */


//You can use  directly in your component's render method to display using {}
const AnotherReactElement_JSXElement = (<><br></br><a href="https://www.google.com">Visit Google</a><br></br></>);
//You must use it as a component (with a capitalized name) in your JSX. usong <xxx/>
const AnotherReactElement_JSX_Functional_Component = ()=> <a href="https://www.google.com">Visit Google</a>;

//This will work because Now we are givinig in a format via React.createElement(..) which is compatiable with React.render
//You can use  directly in your component's render method to display using {}
const RealReactElement = React.createElement(
  "a",
  {
    href: "https://www.google.com",
    target: "_blank",
  },
  "Click React element"
);
/* <MyApp/>  -- Will oviously work its just on same file */
ReactDOM.createRoot(document.getElementById("root")).render(
  /*   AnotherReactElement  - Will work as it is in correct format */
  /*   ReactElement  - Will Not work Not in correct format */

/*   RealReactElement  - This will work as its compatiable payload */
<>
<App/>
<MyApp/>
{MyApp2}
{RealReactElement}
{AnotherReactElement_JSXElement}
<AnotherReactElement_JSX_Functional_Component/>
</>

);

//ReactDOM.createRoot(document.getElementById("root")).render(MyApp2)
//ReactDOM.createRoot(document.getElementById("root")).render(RealReactElement)